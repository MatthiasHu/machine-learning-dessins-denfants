The dataset used in training and testing the neural networks
doesn't seem to be available in a useful format anywhere.
We extract it from the tex source code of Appendix C of the paper.
Here is one way to do this using Linux command line tools.

* Download the tex source from arxive.org.
  [This](https://arxiv.org/e-print/2004.05218) is the direct download link.

* The downloaded file is a .tar.gz file, so extract it:
  ```
  $ tar -xf 2004.05218
  ```

* We only need the file `main.tex`.
  And therein, we only need the adjacency matrices with their captions
  in Appendix C, _The Dessin d'Enfant Dataset_.
  So delete everything up to line 811.

* Remove the accidental line break between lines 601 and 602
  (if you deleted the first 811 lines)
  so every row of every matrix is actually in one line of the file.

* Use `grep -o` to extract the rows of the matrices,
  such as `0 & 1 & 0 & 1 & 1 & 0 & 0 & 0`.
  We also delete the `&` characters using `tr -d`.
  ```
  grep -o '[0-2] & [0-2] & [0-2] & [0-2] & [0-2] & [0-2] & [0-2] & [0-2]' main.tex | tr -d '&' > matrices.txt
  ```
  `matrices.txt` has 1232 lines now,
  representing 154 matrices with 8 rows each.

* To get a similar file of the 154 extension degrees,
  let `grep` search for appropriate strings in the captions
  and then replace these strings by numbers.
  ```
  grep -o 'Q\|sqrt\|cubic\|quartic' main.tex | sed 's/Q/0/g' | sed 's/sqrt/1/g' | sed 's/cubic/2/g' | sed 's/quartic/3/g' > labels.txt
  ```
  `labels.txt` now contains 154 lines with a single number from 0 to 3.
  Note: the label is one less than the extension degree over the rational numbers.
  (Labels need to start from 0 for the Keras library.)
