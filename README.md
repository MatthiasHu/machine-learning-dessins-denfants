We are trying to reproduce the machine learning results of this paper:

[_Machine-Learning Dessins d'Enfants: Explorations via Modular and Seiberg-Witten Curves_](https://arxiv.org/abs/2004.05218)
