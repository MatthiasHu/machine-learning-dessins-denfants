
partition_before_extending = False

extend_by_random_permutations = False
extension_factor = 1000

invalidate_labels = False

original_test_size = 20

number_of_layers = 4
neurons_per_layer = 512
training_epochs = 31


# Preliminary observations:
#
# It only seems possible to reach good accuracy ratings (say, over 0.80)
# in the evaluation on testing data if both partition_before_extending
# and extend_by_random_permutations are set to False.
# Additionally, accuracy does not drop significantly when
# invalidate_labels is set to True.


import tensorflow as tf
from tensorflow import keras
import numpy as np


original_number_of_matrices = 154


# Load matrices and labels.

lines_of_matrices = np.loadtxt("data/matrices.txt")
original_matrices = lines_of_matrices.reshape(original_number_of_matrices, 8, 8)
assert (original_matrices == original_matrices.transpose([0, 2, 1])).all()

original_labels = np.loadtxt("data/labels.txt")
assert original_labels.size == original_number_of_matrices

if (invalidate_labels):
    np.random.shuffle(original_labels)


if (partition_before_extending):

    # Partition into training data and test data.

    shuffled_indices = np.random.permutation(original_number_of_matrices)
    test_indices     = shuffled_indices[:original_test_size]
    training_indices = shuffled_indices[original_test_size:]

    original_test_matrices = original_matrices[test_indices]
    original_test_labels   = original_labels  [test_indices]
    original_training_matrices = original_matrices[training_indices]
    original_training_labels   = original_labels  [training_indices]


# Extend both training and test data
# (or the whole unpartitioned dataset)
# by randomly permuting the matrices.

def permute_matrix(m, p):
    # Permute both rows and columns of a square matrix.
    # (This preserves symmetry of the matrix.)
    return ( m[p, :] )[:, p]

def upto_one_transposition():
    # List 29 permutations of 0..7: identity and single transpositions.
    yield np.arange(8)
    for i in range(8):
        for j in range(i+1, 8):
            p = np.arange(8)
            p[i], p[j] = p[j], p[i]
            yield p

assert len(list(upto_one_transposition())) == 29

def upto_two_transpositions():
    # Actually lists many permutations twice.
    for p1 in upto_one_transposition():
        for p2 in upto_one_transposition():
            yield p1[p2]

def extend_dataset(matrices, labels):
    global extension_factor

    assert matrices.shape[0] == labels.shape[0]
    n = labels.shape[0]

    if (not extend_by_random_permutations):
        extension_factor = 29*29

    new_matrices = np.zeros((n*extension_factor, 8, 8))
    new_labels = np.zeros(n*extension_factor)

    if (extend_by_random_permutations):
        for i in range(n):
            for j in range(extension_factor):
                new_matrices[i * extension_factor + j] = permute_matrix(matrices[i], np.random.permutation(8))
                new_labels[i * extension_factor + j] = labels[i]

    else:
        for i in range(n):
            for j,  permutation in enumerate(upto_two_transpositions()):
                new_matrices[i * extension_factor + j] = permute_matrix(matrices[i], permutation)
                new_labels[i * extension_factor + j] = labels[i]

    return (new_matrices, new_labels)

if (partition_before_extending):
    extended_test_matrices, extended_test_labels = extend_dataset(original_test_matrices, original_test_labels)
    extended_training_matrices, extended_training_labels = extend_dataset(original_training_matrices, original_training_labels)
else:
    extended_matrices, extended_labels = extend_dataset(original_matrices, original_labels)


if (not partition_before_extending):
    extended_test_size = original_test_size * extension_factor

    shuffled_indices = np.random.permutation(original_number_of_matrices * extension_factor)
    test_indices     = shuffled_indices[:extended_test_size]
    training_indices = shuffled_indices[extended_test_size:]

    extended_test_matrices = extended_matrices[test_indices]
    extended_test_labels   = extended_labels  [test_indices]
    extended_training_matrices = extended_matrices[training_indices]
    extended_training_labels   = extended_labels  [training_indices]


# neural network setup

model = keras.Sequential()
model.add(keras.layers.Flatten(input_shape=(8, 8)))
for i in range(number_of_layers):
    model.add(keras.layers.Dense(neurons_per_layer, activation='relu'))
model.add(keras.layers.Dense(4))
model.add(keras.layers.Softmax())

model.compile(
      optimizer='adam'
    , loss=keras.losses.SparseCategoricalCrossentropy()
    , metrics=['accuracy']
    )


# train neural network

print()
print("START OF TRAINING")
print()
              
model.fit(
      extended_training_matrices
    , extended_training_labels
    , epochs = training_epochs
    )


# evaluate accuracy of the neural network on the test data

print()
print("EVALUATION ON TEST DATA")
print()

model.evaluate(
      extended_test_matrices
    , extended_test_labels
    )
